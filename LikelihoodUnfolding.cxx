
#include <RooAbsReal.h>
#include <RooPoisson.h>
#include "Utils.cxx"

using namespace RooFit;

  
void LikelihoodUnfolding() {

  // Define the regularisation parameter.
  Double_t tau = 0.0001;

  std::string inputFile("histograms_sys_20reco20truth.root");
  
  TFile input(inputFile.c_str());

  TH1D* truthHist = (TH1D*)input.Get("Nom/truth");
  TH1D* recoHist = (TH1D*)input.Get("Nom/reco");
  TH1D* dataHist = (TH1D*)input.Get("Nom/data");
  TH1D* truthTestHist = (TH1D*)input.Get("Nom/truth_test");
  TH2D* respHist = (TH2D*)input.Get("Nom/response");
  
  Int_t n_reco_bins = dataHist->GetNbinsX();
  Int_t n_truth_bins = truthHist->GetNbinsX();
  
  RooArgList dataBinVars;
  RooArgList recoBinVars;
  RooArgList truthBinVars;
  RooArgList pdfList;

  // Get a constant RooRealVar for each response matrix element and
  // set its value.
  std::vector<RooArgList> Rij;
  getResponse(Rij, respHist, truthHist);  
  
  // Create a list with variables for all the truth bins.
  for (int i = 0; i < n_truth_bins; i++){

    std::string truthBinName("mu_");
    truthBinName.append(std::to_string(i));
    
    RooRealVar* truthBinVar = new RooRealVar(truthBinName.c_str(),truthBinName.c_str(),truthTestHist->GetBinContent(i+1),0,100000000);
    truthBinVar->setError(1);
    
    truthBinVars.add(*truthBinVar);
  }
  
  // Each RooFormulaVar defines each reco bin in terms
  // of response matrix elements and truth bins i.e.
  // nu_{i} = Sum_{j=1} R_{ij}mu{j}
  std::vector<RooFormulaVar> nuFormulas;

  // Each RooFormulaVar defines the Taylor expansion
  // of each element of the response matrix in terms
  // of the nuisance parameters.
  std::vector<std::vector<RooFormulaVar>> R_thetas;

  // A list with the nuisance parameter variables.
  RooArgList NPVars;

  // Create the variables for the nuisance parameters.
  RooRealVar* NP1 = new RooRealVar("theta0","theta0",0,-1,1);
  RooRealVar* NP2 = new RooRealVar("theta1","theta1",0,-1,1);

  // These values represent an auxiliary measurement or theoretical guess
  // with an uncertainty on it for each nuisance parameter.
  RooRealVar* NP1_mean = new RooRealVar("theta0_mean","theta0_mean",0);
  RooRealVar* NP2_mean = new RooRealVar("theta1_mean","theta1_mean",0);
  RooRealVar* NP1_sig = new RooRealVar("theta0_sig","theta0_sig",0.5);
  RooRealVar* NP2_sig = new RooRealVar("theta1_sig","theta1_sig",0.5);

  
  NPVars.add(*NP1);
  NPVars.add(*NP2);
  ((RooRealVar*)NPVars.at(0))->setError(0.001);
  ((RooRealVar*)NPVars.at(1))->setError(0.001);
  
  // Create the formulas.
  makeNuFormulaWithNPs(truthBinVars, NPVars, nuFormulas, inputFile);

  TMatrixD response(n_reco_bins, n_truth_bins);
  TVectorD truthVec(n_truth_bins);
  TVectorD effVec(n_truth_bins);

  
  
  for (int i = 0; i < n_reco_bins; i++){
    truthVec(i) = truthHist->GetBinContent(i+1);
    for (int j = 0; j < n_truth_bins; j++){      
      response(i,j) = ((RooRealVar*)Rij.at(i).at(j))->getVal();
    }
  }
  TVectorD recoVec = response*truthVec;  
  

  // Define the NLL constraint term formula that constrains on smoothness:
  // S(mu) = - tau * Sum_{i}^{M-2}(-mu_{i} + 2mu_{i+1} - mu_{i+2})^{2}
  RooFormulaVar Smu = makeTikhonovFormula(truthBinVars,tau);

  // Define the NLL constraint term formula that constrains on the difference between
  // the truth parameters and a truth benchmark:
  // S(mu) = tau * Sum_{i}^{M}(mu_{i} - muBench_{i})^{2}
  //RooFormulaVar Smu = makeTruthDiffConstr(truthBinVars,truthVec,tau);
  
  // Loop over all the truth bins and construct a Poisson pdf for each.
  for (int i = 0; i < n_reco_bins; i++){

    std::string pdfName("poisPdf_bin_");
    std::string dataBinName("n_");
    
    pdfName.append(std::to_string(i));
    dataBinName.append(std::to_string(i));

    // Create a variable for the data bin count.
    RooRealVar* dataBinVar = new RooRealVar(dataBinName.c_str(),dataBinName.c_str(),dataHist->GetBinContent(i+1),0,1e20);

    // Create a Poisson p.d.f. for the likelihood.
    RooPoisson* poisPdf = new RooPoisson(pdfName.c_str(),pdfName.c_str(),*dataBinVar,nuFormulas.at(i));

    dataBinVars.add(*dataBinVar);

    pdfList.add(*poisPdf);
  }

  // Add Gaussian constraint terms for the nuisance parameters.
  RooGaussian* theta0Constr = new RooGaussian("theta0","theta0",*NP1_mean,*NP1,*NP1_sig);
  RooGaussian* theta1Constr = new RooGaussian("theta1","theta1",*NP2_mean,*NP2,*NP2_sig);

  // Add the Gaussian constraint terms to the pdf list.
  pdfList.add(*theta0Constr);
  pdfList.add(*theta1Constr);
  
  // Take the product of all pdfs.
  RooProdPdf LH("Likelihood","Likelihood",pdfList);
  
  // Make a dataset for all the data bin variables.
  RooDataSet in("indata","indata",dataBinVars);
  
  // Add this one datapoint.
  in.add(dataBinVars);
  
  // Create the negative log-likelihood.
  RooAbsReal* nll = LH.createNLL(in);

  // Add the constraint term to the NLL.
  RooFormulaVar nllConstr("constrNLL","@0 + @1",RooArgList(*nll,Smu));  
  
  // Create a minuit instance.
  RooMinuit *roomin = new RooMinuit(nllConstr);

  // Define the computational rigorousness:
  // (1=fast,2=slow but more accurate,3=intermediate)
  roomin->setStrategy(2);

  // Set the print level output.
  roomin->setPrintLevel(0);
  
  // Minimize.
  roomin->migrad();

  // Print the pull.
  printPull(truthBinVars, truthTestHist);
  
  input.Close();

}
