# LikelihoodUnfolding

A Likelihood-based unfolding algorithm. This is a simple ROOT-macro that unfolds by minimizing the negative log-likelihood constructed from Poisson P.D.F.'s and a Tikhonov constraint term.
