void calcEfficiencies(TVectorD& eff, TH2D*& respHist, TH1D*& truthHist){

  for (int i = 0; i < truthHist->GetNbinsX(); i++){
    Double_t recoSum = 0;
    for (int j = 0; j < respHist->GetNbinsX()+2; j++){
      recoSum += respHist->GetBinContent(j, i+1);
    }
    eff(i) = recoSum / truthHist->GetBinContent(i+1);
  }
}


void printPull(RooArgList& truthBinVars, TH1D* truthHist){

  Int_t i = 0;
  
  for (auto truthBin : truthBinVars){
    
    RooRealVar* truthParam = (RooRealVar*)truthBin;
    std::cout << "Bin " << i << " pull= " << (truthParam->getVal() - truthHist->GetBinContent(i+1)) / truthParam->getError() << " truth est.= " << truthParam->getVal() << " truth bench.= " << truthHist->GetBinContent(i+1) << " truth error= " << truthParam->getError() << std::endl;
    i++;
  }
}

RooFormulaVar makeTruthDiffConstr(RooArgList& mu, TVectorD truthBias, Double_t tau){

  std::string formulaString;
  formulaString.append(std::to_string(tau));
  formulaString.append(" * (");

  RooArgList S;
  
  Int_t i = 0;
  
  while(mu.at(i)){

    S.add(*(mu.at(i)));

    if (i!=0) formulaString.append("+");
    formulaString.append("(@");
    formulaString.append(std::to_string(i));
    formulaString.append("-");
    formulaString.append(std::to_string(truthBias(i)));

    formulaString.append(")*(");

    formulaString.append("@");
    formulaString.append(std::to_string(i));
    formulaString.append("-");
    formulaString.append(std::to_string(truthBias(i)));

    formulaString.append(")");
    
    i++;
  }

  formulaString.append(")");

  return RooFormulaVar("S",formulaString.c_str(),S);
}

RooFormulaVar makeTikhonovFormula(RooArgList& mu, Double_t tau){

  std::string formulaString;
  formulaString.append(std::to_string(tau));
  formulaString.append(" * (");

  RooArgList S;
  
  Int_t i = 0;
  while(mu.at(i)){
    S.add(*(mu.at(i)));
    i++;
  }

  i = 0;
  while(mu.at(i+2)){

    if (i!=0) formulaString.append("+");
    formulaString.append("(-@");
    formulaString.append(std::to_string(i));
    formulaString.append("+2*");
    formulaString.append("@");
    formulaString.append(std::to_string(i+1));
    formulaString.append("-@");
    formulaString.append(std::to_string(i+2));

    formulaString.append(")*(");

    formulaString.append("-@");
    formulaString.append(std::to_string(i));
    formulaString.append("+2*");
    formulaString.append("@");
    formulaString.append(std::to_string(i+1));
    formulaString.append("-@");
    formulaString.append(std::to_string(i+2));
    formulaString.append(")");
    
    i++;
  }

  formulaString.append(")");
  
  return RooFormulaVar("S",formulaString.c_str(),S);
  
}


void getResponse(std::vector<RooArgList>& Rij, TH2D* respHist, TH1D* truthHist){

  for (int i = 0; i < respHist->GetNbinsX(); i++){
    RooArgList nu_i;
    Rij.push_back(nu_i);
  }
  
  
  for (int i = 0; i < respHist->GetNbinsY(); i++){
    
    double rsum = 0;
    for (int j = 0; j < respHist->GetNbinsX() + 2; j++){
      rsum += respHist->GetBinContent(j, i+1);
    }
    
    for (int j = 0; j < respHist->GetNbinsX(); j++){

      std::string rname("response_");
      rname.append(std::to_string(j));
      rname.append(std::to_string(i));

      double rval = respHist->GetBinContent(j+1,i+1) / rsum;

      
      RooRealVar* rij = new RooRealVar(rname.c_str(),rname.c_str(),rval);
      
      Rij.at(j).add(*rij);
    }
  }
}

void getNPDerivativeMatrices(TH2D* nom_hist, std::vector<TH2D*>& up_hists, std::vector<TH2D*>& down_hists, std::vector<std::vector<std::vector<RooRealVar*>>>& NPDerMat){
  
  if (up_hists.size() != down_hists.size()){
    std::cout << "Error: number of varied response histogram templates not the same for up as down." << std::endl;
    return;
  }

  // Normalize the histograms to a response matrix.
  for (int i = 0; i < nom_hist->GetNbinsY(); i++){

    double nom_sum = 0;
    std::vector<double> NP_up_sums;
    std::vector<double> NP_down_sums;
    for(int NP_i = 0; NP_i < up_hists.size(); NP_i++){
      NP_up_sums.push_back(0);
      NP_down_sums.push_back(0);
    }

    // Sum over all truth bins.
    for (int j = 0; j < nom_hist->GetNbinsX() + 2; j++){
      nom_sum += nom_hist->GetBinContent(j, i+1);

      for(int NP_i = 0; NP_i < up_hists.size(); NP_i++){
	NP_up_sums.at(NP_i) = NP_up_sums.at(NP_i) + up_hists.at(NP_i)->GetBinContent(j,i+1);
	NP_down_sums.at(NP_i) = NP_down_sums.at(NP_i) + down_hists.at(NP_i)->GetBinContent(j,i+1);
      }      
    }
    
    // Normalize. 
    for (int j = 0; j < nom_hist->GetNbinsX(); j++){   
      nom_hist->SetBinContent(j+1,i+1, nom_hist->GetBinContent(j+1,i+1) / nom_sum );

      for(int NP_i = 0; NP_i < up_hists.size(); NP_i++){
	up_hists.at(NP_i)->SetBinContent(j+1, i+1, up_hists.at(NP_i)->GetBinContent(j+1, i+1) / NP_up_sums.at(NP_i));
	down_hists.at(NP_i)->SetBinContent(j+1, i+1, down_hists.at(NP_i)->GetBinContent(j+1, i+1) / NP_down_sums.at(NP_i));
      }      
    }
  }

    
  // Calculate the derivatives with finite differences.
  for (int i = 0; i < nom_hist->GetNbinsX(); i++){
    std::vector<std::vector<RooRealVar*>> input_col;
    for (int j = 0; j < nom_hist->GetNbinsY(); j++){
      std::vector<RooRealVar*> vec_der;
      for (int NP_i = 0; NP_i < up_hists.size(); NP_i++){

	Double_t dRdTheta =  (up_hists.at(NP_i)->GetBinContent(i+1, j+1) - down_hists.at(NP_i)->GetBinContent(i+1, j+1))/2;
	
	std::string rname("dR_");
	rname.append(std::to_string(i));
	rname.append(std::to_string(j));
	rname.append("/dTheta_");
	rname.append(std::to_string(NP_i));
	
	RooRealVar* NPDer = new RooRealVar(rname.c_str(),rname.c_str(),dRdTheta);
	vec_der.push_back(NPDer);
      }
      input_col.push_back(vec_der);
    }
    NPDerMat.push_back(input_col);
  }  
  
}

// This builds the nu(reco bin count) as function of the truth bin counts and
// nuisance parameters.
void makeNuFormulaWithNPs(RooArgList& mu, RooArgList& thetas, std::vector<RooFormulaVar>& nuFormulas, std::string& inputFile){

  TFile input(inputFile.c_str());

  TH1D* truth_nom = (TH1D*)input.Get("Nom/truth");
  
  TH2D* resp_nom = (TH2D*)input.Get("Nom/response");
  
  TH2D* resp_NP_1_up = (TH2D*)input.Get("NP1/response/up");
  TH2D* resp_NP_1_down = (TH2D*)input.Get("NP1/response/down");
  
  TH2D* resp_NP_2_up = (TH2D*)input.Get("NP2/response/up");
  TH2D* resp_NP_2_down = (TH2D*)input.Get("NP2/response/down");

  std::vector<TH2D*>up_hists;
  std::vector<TH2D*>down_hists;

  up_hists.push_back(resp_NP_1_up);
  up_hists. push_back(resp_NP_2_up);
  down_hists.push_back(resp_NP_1_down);
  down_hists.push_back(resp_NP_2_down);

  TVectorD efficiencies(truth_nom->GetNbinsX());

  // Calculate the efficiencies from the response matrix and
  // the truth histogram both made from the same truth MC events.
  calcEfficiencies(efficiencies, resp_nom, truth_nom);
  
  std::vector<std::vector<std::vector<RooRealVar*>>> NP_derivatives;

  // Calculate the derivatives of each response matrix element w.r.t.
  // each nuisance parameter.
  getNPDerivativeMatrices(resp_nom, up_hists, down_hists, NP_derivatives);
  
  for (int i = 0; i < resp_nom->GetNbinsX(); i++){

    
    Int_t param_count = 0;
    
    RooArgList nu;
    
    // A string is constructed that represents the sum of response
    // matrix elements and truth bins. Floating parameters are denoted
    // with @ and are denoted with the index of the RooRealVar in the
    // RooArgList that is passed to RooFormulaVar.
    // nu_{i} = Sum_{j} R(theta)_{ij} * mu_{j} * eff_{j}
    //        = R(theta)_i1 * mu_1 * eff_1 + ...
    //        = ( R(theta_nom)_i1 + theta_1 * dR(theta_1)_i1/dtheta_1 + ...) * mu_1 * eff_1 + ...
    //        = ( R(theta_nom)_i1 * mu_1 * eff_1 + theta_1 * dR(theta_1)_i1/dtheta_1 * mu_1 * eff_1 + theta_2 * dR(theta_2)_i1/dtheta_2 * mu_1 * eff_1 + ...
    //        = ( @0 * @1 * 0.83 + @2 * @3 * @4 * 0.83 + @5 * @6 * @7 * 0.83 +...

    // Start of the formula string.
    std::string formulaString("");
    Int_t j = 0;
    while (mu.at(j)){

      std::string rname("dR_");
      rname.append(std::to_string(i));
      rname.append(std::to_string(j));

      
      // Convert the nominal response matrix element value to a string.
      RooRealVar* R_nom = new RooRealVar(rname.c_str(),rname.c_str(), resp_nom->GetBinContent(i+1,j+1));
      formulaString.append("@");
      formulaString.append(std::to_string(param_count));
      param_count++;
      nu.add(*R_nom);

      // Multiply with truth bin.
      formulaString.append(" * ");
      formulaString.append("@");
      formulaString.append(std::to_string(param_count));
      param_count++;
      
      nu.add(*(mu.at(j)));

      // Multiply with the efficiency.
      formulaString.append(" * ");
      formulaString.append(std::to_string(efficiencies(j)));
      
      // Add the derivatives products as well.
      for (int k = 0; k < NP_derivatives.at(0).at(0).size(); k++){
	formulaString.append(" + ");
	formulaString.append("@");
	formulaString.append(std::to_string(param_count));
	nu.add(*(NP_derivatives.at(i).at(j).at(k)));
	param_count++;
	
	formulaString.append(" * @");
	formulaString.append(std::to_string(param_count));
	nu.add(*(thetas.at(k)));

	param_count++;

	formulaString.append(" * ");
	formulaString.append("@");
	formulaString.append(std::to_string(param_count));
	param_count++;
	nu.add(*(mu.at(j)));

	// Multiply with the efficiency.
	formulaString.append(" * ");
	formulaString.append(std::to_string(efficiencies(j)));
      }

      formulaString.append(" + ");
      j++;
    }
    formulaString.resize (formulaString.size () - 3);

    std::string nu_name("nu_formula_");
    nu_name.append(std::to_string(i));

    RooFormulaVar nu_formula(nu_name.c_str(), formulaString.c_str(), nu);

    nuFormulas.push_back(nu_formula);
  }

  input.Close();
}


